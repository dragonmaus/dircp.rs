mod io_channel;

pub use crate::io_channel::*;
use std::{
    fs::{self, DirEntry},
    io,
};

pub fn scan_directory(path: &str) -> io::Result<Vec<String>> {
    let mut paths: Vec<String> = Vec::new();

    paths.push(path.to_string());

    if fs::metadata(path)?.is_dir() {
        let mut entries: Vec<DirEntry> = Vec::new();
        for entry in fs::read_dir(&path)? {
            entries.push(entry?);
        }
        let entries = entries.as_mut_slice();
        entries.sort_unstable_by_key(|k| k.path());

        for entry in entries {
            for path in scan_directory(&entry.path().to_string_lossy())? {
                paths.push(path);
            }
        }
    }

    Ok(paths)
}
