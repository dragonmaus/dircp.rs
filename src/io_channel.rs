use std::{
    io::{self, Read, Write},
    sync::mpsc,
};

pub fn new_io_channel() -> (WriteSender, ReadReceiver) {
    let (sender, receiver) = mpsc::channel();

    (WriteSender::new(sender), ReadReceiver::new(receiver))
}

pub struct ReadReceiver {
    inner: mpsc::Receiver<u8>,
}

impl ReadReceiver {
    pub fn new(inner: mpsc::Receiver<u8>) -> ReadReceiver {
        ReadReceiver { inner }
    }
}

impl Read for ReadReceiver {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        if buf.is_empty() {
            return Ok(0);
        }

        let mut i = 0;
        let mut iter = self.inner.iter();
        loop {
            if i >= buf.len() {
                break;
            }
            match iter.next() {
                None => break,
                Some(byte) => buf[i] = byte,
            }
            i += 1;
        }

        Ok(i)
    }
}

pub struct WriteSender {
    inner: mpsc::Sender<u8>,
}

impl WriteSender {
    pub fn new(inner: mpsc::Sender<u8>) -> WriteSender {
        WriteSender { inner }
    }
}

impl Write for WriteSender {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let mut count = 0;

        for byte in buf {
            self.inner.send(*byte).unwrap();
            count += 1;
        }

        Ok(count)
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}
