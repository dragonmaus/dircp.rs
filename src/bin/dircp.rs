use dircp::*;
use getopt::prelude::*;
use my::{program_main, util};
use std::{cmp::Ordering, env, error::Error, fs, io, thread};

program_main!("dircp");

fn usage_line() -> String {
    format!("Usage: {} [-hv] from to", util::program_name("dircp"))
}

fn print_usage() -> Result<i32, Box<dyn Error>> {
    println!("{}", usage_line());
    println!("  -h   display this help");
    println!("  -v   verbose output");

    Ok(0)
}

fn program() -> Result<i32, Box<dyn Error>> {
    let mut args = util::program_args();
    let mut opts = Parser::new(&args, "hv");

    let mut verbose = false;
    loop {
        match opts.next().transpose()? {
            None => break,
            Some(opt) => match opt {
                Opt('h', None) => return print_usage(),
                Opt('v', None) => verbose = true,
                _ => unreachable!(),
            },
        }
    }

    let current_dir = env::current_dir()?.canonicalize()?;

    let args = args.split_off(opts.index());
    match args.len().cmp(&2) {
        Ordering::Greater => {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                format!("Too many arguments\n{}", usage_line()),
            )
            .into())
        }
        Ordering::Less => {
            return Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                format!("Missing argument(s)\n{}", usage_line()),
            )
            .into())
        }
        _ => (),
    };
    let from = current_dir.join(&args[0]);
    let to = current_dir.join(&args[1]);

    let from_metadata = fs::metadata(&from).map_err(|e| {
        io::Error::new(
            e.kind(),
            format!(
                "unable to open {}: {}",
                from.to_string_lossy().into_owned(),
                e
            ),
        )
    })?;

    if !from_metadata.is_dir() {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            format!(
                "unable to copy {}: Not a directory",
                from.to_string_lossy().into_owned()
            ),
        )
        .into());
    }

    fs::create_dir_all(&to).map_err(|e| {
        io::Error::new(
            e.kind(),
            format!(
                "unable to create {}: {}",
                to.to_string_lossy().into_owned(),
                e
            ),
        )
    })?;

    env::set_current_dir(from)?;

    let (sender, receiver) = new_io_channel();

    let reader = thread::spawn(move || {
        let mut builder = tar::Builder::new(sender);
        builder.follow_symlinks(false);
        builder.mode(tar::HeaderMode::Complete);

        for entry in scan_directory(".").unwrap() {
            if verbose {
                println!("{}", &entry);
            }
            builder.append_path(&entry).unwrap();
        }
        builder.finish().unwrap()
    });

    let writer = thread::spawn(move || {
        let mut archive = tar::Archive::new(receiver);
        archive.set_preserve_mtime(true);
        archive.set_preserve_permissions(true);
        archive.set_unpack_xattrs(true);

        archive.unpack(to).unwrap();
    });

    reader.join().unwrap();
    writer.join().unwrap();

    Ok(0)
}
